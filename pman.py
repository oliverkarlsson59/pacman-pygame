import pygame, sys, time, random
pygame.mixer.init()

##Global values
RIGHT = [1, 0]
LEFT = [-1, 0]
UP = [0, -1]
DOWN = [0, 1]
PASSIVE = [0,0]

IMGWIDTH = 17
IMGHEIGHT = 17
##
##Setting up sounds
pygame.mixer.set_num_channels(8)
soundChannel = pygame.mixer.Channel(5)
pacman_intro = pygame.mixer.Sound("assets/pacman/pacman_beginning.wav")
pacman_eat_fruit = pygame.mixer.Sound("assets/pacman/pacman_eatfruit.wav")
pacman_eat_ghost = pygame.mixer.Sound("assets/pacman/pacman_eatghost.wav")
pacman_chomp = pygame.mixer.Sound("assets/pacman/pacman_chomp.wav")
pacman_death = pygame.mixer.Sound("assets/pacman/pacman_death.wav")
pygame.mixer.music.set_volume(0.1)
##
##Loading and scaling all images
ImgPacmanPassive = pygame.image.load("assets/pacman/pacman3.png")
ImgPacmanPassive = pygame.transform.scale(ImgPacmanPassive, (IMGWIDTH, IMGHEIGHT))

ImgPacmanUpOpen = pygame.image.load("assets/pacman/pacman6.png")
ImgPacmanUpOpen = pygame.transform.scale(ImgPacmanUpOpen, (IMGWIDTH, IMGHEIGHT))

ImgPacmanUpClosed = pygame.image.load("assets/pacman/pacman7.png")
ImgPacmanUpClosed = pygame.transform.scale(ImgPacmanUpClosed, (IMGWIDTH, IMGHEIGHT))

ImgPacmanLeftOpen = pygame.image.load("assets/pacman/pacman4.png")
ImgPacmanLeftOpen = pygame.transform.scale(ImgPacmanLeftOpen, (IMGWIDTH, IMGHEIGHT))
ImgPacmanLeftClosed = pygame.image.load("assets/pacman/pacman5.png")
ImgPacmanLeftClosed = pygame.transform.scale(ImgPacmanLeftClosed, (IMGWIDTH, IMGHEIGHT))
ImgPacmanRightOpen = pygame.image.load("assets/pacman/pacman1.png")
ImgPacmanRightOpen = pygame.transform.scale(ImgPacmanRightOpen, (IMGWIDTH, IMGHEIGHT))
ImgPacmanRightClosed = pygame.image.load("assets/pacman/pacman2.png")
ImgPacmanRightClosed = pygame.transform.scale(ImgPacmanRightClosed, (IMGWIDTH, IMGHEIGHT))

ImgPacmanDownOpen = pygame.image.load("assets/pacman/pacman8.png")
ImgPacmanDownOpen = pygame.transform.scale(ImgPacmanDownOpen, (IMGWIDTH, IMGHEIGHT))
ImgPacmanDownClosed = pygame.image.load("assets/pacman/pacman9.png")
ImgPacmanDownClosed = pygame.transform.scale(ImgPacmanDownClosed, (IMGWIDTH, IMGHEIGHT))

ImgVulnerableGhost = pygame.image.load("assets/pacman/vulnerableghost.png")
ImgVulnerableGhost = pygame.transform.scale(ImgVulnerableGhost, (IMGWIDTH, IMGHEIGHT))

ImgVulnerableGhostEnding = pygame.image.load("assets/pacman/vulnerableghostending.png")
ImgVulnerableGhostEnding = pygame.transform.scale(ImgVulnerableGhostEnding, (IMGWIDTH, IMGHEIGHT))

ImgBlinky = pygame.image.load("assets/pacman/Blinky.png")
ImgBlinky = pygame.transform.scale(ImgBlinky, (IMGWIDTH, IMGHEIGHT))
ImgPinky = pygame.image.load("assets/pacman/Pinky.png")
ImgPinky = pygame.transform.scale(ImgPinky, (IMGWIDTH, IMGHEIGHT))
ImgInky = pygame.image.load("assets/pacman/Inky.png")
ImgInky = pygame.transform.scale(ImgInky, (IMGWIDTH, IMGHEIGHT))
ImgClyde = pygame.image.load("assets/pacman/Clyde.png")
ImgClyde = pygame.transform.scale(ImgClyde, (IMGWIDTH, IMGHEIGHT))


ImgFruit = pygame.image.load("assets/pacman/fruit2.png")
ImgSmallFruit = pygame.transform.scale(ImgFruit, (5, 5))
ImgBigFruit = pygame.transform.scale(ImgFruit, (15, 15))
####

class Player(pygame.sprite.Sprite):
    speed = [0, 0]
    status = 0
    stepCounter = 0
    color = pygame.Color(255, 255, 255)
    character = ""
    score = 0
    combo = 0

    def __init__(self, color, x, y, character):
        pygame.sprite.Sprite.__init__(self)
        self.image = pygame.Surface([IMGHEIGHT, IMGWIDTH])
        self.rect = self.image.get_rect()
        self.rect.x = x
        self.rect.y = y
        self.color = color
        self.character = character
        self.setImage()

    def setImage(self):
        if self.character == "Pacman":
            if self.speed == PASSIVE:
                self.image = ImgPacmanPassive
                return
            if self.speed == UP:
                if self.stepCounter % 15 > 9:
                    self.image = ImgPacmanUpOpen
                else:
                    self.image = ImgPacmanUpClosed
                return
            if self.speed == LEFT:
                if self.stepCounter % 15 > 9:
                    self.image = ImgPacmanLeftOpen
                else:
                    self.image = ImgPacmanLeftClosed
               
                return
            if self.speed == RIGHT:
                if self.stepCounter % 15 > 9:
                    self.image = ImgPacmanRightOpen
                else:
                    self.image = ImgPacmanRightClosed
                
                return
            if self.speed == DOWN:
                if self.stepCounter % 15 > 9:
                    self.image = ImgPacmanDownOpen
                else:
                    self.image = ImgPacmanDownClosed
                return

        if player.status > 0:
            if player.status < 250:
                self.image = ImgVulnerableGhostEnding
            else:
                self.image = ImgVulnerableGhost
            return
        if self.character == "Blinky":
            self.image = ImgBlinky
            return
        if self.character == "Pinky":
            self.image = ImgPinky
            return
        if self.character == "Inky":
            self.image = ImgInky
            return
        if self.character == "Clyde":
            self.image = ImgClyde
            return

    def update(self, walls, gate, ghosts):
        #get old positions
        oldX = self.rect.x
        oldY = self.rect.y

        #move player
        self.rect = self.rect.move(self.speed)

        #check if we collided with a wall and revert to old position if we did
        didCollide = pygame.sprite.spritecollide(self, walls, False)
        if didCollide:
            self.rect.x = oldX
            self.rect.y = oldY
        if self.character == "Pacman":
            didCollideWithGate = pygame.sprite.spritecollide(self, gate, False)
            if didCollideWithGate:
                self.rect.x = oldX
                self.rect.y = oldY
        
        #if a player steps outside the area teleport them to the other side
        if self.rect.x < 0:
            self.rect.x = 400
        if self.rect.x > 400:
           self.rect.x = 0
        if self.rect.y < 0:
            self.rect.y = 390
        if self.rect.y > 400:
            self.rect.y = 0

        #check if we currently have the effect of a fruit
        if self.status > 0:
            #self.image.fill(pygame.Color(0, 0, 255))
            self.status -= 1
        else:
            self.combo = 0

        for index, ghost in enumerate(ghosts):
            if self.rect.colliderect(ghost.rect):
                if self.status > 0:
                    pygame.mixer.music.stop()
                    pygame.mixer.Sound.play(pacman_eat_ghost)
                    pygame.mixer.music.stop()
                    self.combo += 1
                    self.score += 200 * self.combo
                    time.sleep(1 / 3)
                    ghost.respawn()
                else:
                    pygame.mixer.Sound.play(pacman_death)
                    pygame.mixer.music.stop()
                    time.sleep(3)
                    sys.exit()
        self.stepCounter += 1
        self.setImage()
        
        
    def moveLeft(self):
        self.speed = [1, 0]

    def moveRight(self):
        self.speed = [-1, 0]

    def moveUp(self):
        self.speed = [0, -1]

    def moveDown(self):
        self.speed = [0, 1]

    def standStill(self):
        self.speed = [0, 0]


class Ghost(Player):
    turnCounter = 100
    def randomizeMovement(self):
        if self.turnCounter == 0:
            self.turnCounter = 100
            x = random.randint(-3, 3)
            if x == 0:
                y = random.randint(-3, 3)
                self.speed = [0, y]
            else:
                self.speed = [x, 0]
        else:
            self.turnCounter -= 1
        self.update(wall_list,[], [])

    def respawn(self):
        self.rect.x = 200
        self.rect.y = 200
        self.randomizeMovement()
        
class Wall(pygame.sprite.Sprite):

    def __init__(self, color, width, height, x, y):
        pygame.sprite.Sprite.__init__(self)
        self.image = pygame.Surface([width, height])
        self.image.fill(color)
        self.rect = self.image.get_rect()
        self.rect.x = x
        self.rect.y = y

def createWalls():
    walls = [
        (10, 130, 0, 0), # upper left side wall
        (80, 10, 0, 125),
        (10, 50, 70, 125),
        (80, 10, 0, 170),

        (10, 130, 0, 265), #lower left side wall
        (80, 10, 0, 220),
        (10, 50, 70, 220),
        (80, 10, 0, 265),
        
        (10, 130, 390, 0), #upper right side wall
        (80, 10, 320, 125),
        (10, 50, 320, 125),
        (90, 10, 320, 170),
        

        (10, 130, 390, 265), #lower right side wall
        (80, 10, 320, 220),
        (10, 55, 320, 220),
        (90, 10, 320, 265),

        (400, 10, 0, 0), #top wall
        (400, 10, 0, 390), #bottom wall
        ### upper half
        (50, 30,  30, 30),
        (60, 30,  110, 30),
        (10, 60, 195, 0),
        (60, 30,  230, 30),
        (50, 30,  320, 30),

        (50, 20,  30, 80),
        (10, 100,  110, 80),
        (60, 10,  110, 125),

        (100, 10,  150, 80),
        (10, 55,  195, 80),

        (10, 100,  280, 80),
        (60, 10,  230, 125),

        (50, 20,  320, 80),
        ###
        #center box
        #(30, 10, 150, 165),
        #(30, 10, 230, 165),
        (100, 10, 150, 220),
        (10, 65, 150, 165),
        (10, 65, 250, 165),
        #
        ###lower half
        

        (50, 20,  30, 300),
        (10, 100,  110, 220),
        (60, 10,  110, 265),

        (100, 10,  150, 310),
        (10, 50,  195, 265),

        (10, 100,  280, 220),
        (60, 10,  230, 265),

        (50, 20,  320, 300),

        (50, 30,  30, 340),
        (60, 30,  110, 340),
        (10, 60, 195, 340),
        (60, 30,  230, 340),
        (50, 30,  320, 340),
        ###
    ]
    return walls

class Fruit(pygame.sprite.Sprite):
    def __init__(self, color, width, height, x, y, size):
        pygame.sprite.Sprite.__init__(self)
        self.image = pygame.Surface([width, height])
        if size == "small":
            self.image = ImgSmallFruit
        else:
            self.image = ImgBigFruit
        self.rect = self.image.get_rect()
        self.rect.x = x
        self.rect.y = y
    
    def despawn(self):
        self.rect.x = 700
        self.rect.y = 700



def playerCollideWithFruit(fruits, player):
    for index, fruit in enumerate(fruits):
        if player.rect.colliderect(fruit.rect):
            if soundChannel.get_busy():
                pass
            else:
                soundChannel.play(pacman_chomp)
                #soundChannel.stop()
            player.score += 10
            fruit.despawn()

def playerCollideWithBigFruit(fruits, player):
    for index, fruit in enumerate(fruits):
            if player.rect.colliderect(fruit.rect):
                if soundChannel.get_busy():
                    pass
                else:
                    soundChannel.play(pacman_eat_fruit)
                player.status = 1000
                player.score += 40
                fruit.despawn()

def updateScreen(player):
    screen.fill(black)
    all_sprites_list.draw(screen)
    textsurface = myfont.render('Score: ' + str(player.score), False, (0, 255, 0))
    
    player.update(wall_list, gate_list, ghost_list)
    screen.blit(textsurface,(0,430))
    pygame.display.flip()
    time.sleep (100.0 / 10000.0);

pygame.init()
pygame.font.init()
myfont = pygame.font.SysFont('Comic Sans MS', 30)

size = width, height = 400, 500
black = 0, 0, 0

screen = pygame.display.set_mode(size)
player = Player(pygame.Color(255, 255, 0), 190, 235, "Pacman")

all_sprites_list = pygame.sprite.Group()
wall_list = pygame.sprite.RenderPlain()
ghost_list = pygame.sprite.RenderPlain()

walls = createWalls()
for wall in walls:
    block = Wall(pygame.Color(0,0,255), wall[0], wall[1], wall[2], wall[3])
    wall_list.add(block)
    all_sprites_list.add(block)

all_sprites_list.add(player)

blinky = Ghost(pygame.Color(255, 0, 0), 180, 180, "Blink")
pinky = Ghost(pygame.Color(255, 20, 147),180, 180, "Pinky")
inky = Ghost(pygame.Color(0, 255, 255), 180, 180, "Inky")
clyde = Ghost(pygame.Color(255, 165, 0), 180, 180, "Clyde")

ghost_list.add(blinky)
ghost_list.add(pinky)
ghost_list.add(inky)
ghost_list.add(clyde)

all_sprites_list.add(blinky)
all_sprites_list.add(pinky)
all_sprites_list.add(inky)
all_sprites_list.add(clyde)


fruit_list = pygame.sprite.RenderPlain()
for x in range(1, 40):
    for y in range(1, 40):
        fruit = Fruit(pygame.Color(255, 255, 0), 25, 25, x*18, y*17.5, "small")
        doesCollideWithWall = pygame.sprite.spritecollide(fruit, wall_list, False)
        doesCollideWithFruit = pygame.sprite.spritecollide(fruit, fruit_list, False)
        if not doesCollideWithFruit and not doesCollideWithWall and not(100<= x*18 <=290 and 135 <= y*17.5 <=250) and not(310<= x*18 <=400 and 135 <= y*17.5 <=250) and not(0<= x*18 <=80 and 135 <= y*17.5 <=250) and y*17.5 < 400:
            all_sprites_list.add(fruit)
            fruit_list.add(fruit)
        else:
            pass


big_fruit_list = pygame.sprite.Group()
big_fruit_list.add(Fruit((255, 255, 0), 15, 15, 15, 15, "big"),Fruit((255, 255, 0), 15, 15, 375, 15, "big"),Fruit((255, 255, 0), 15, 15, 15, 375, "big"),Fruit((255, 255, 0), 15, 15, 375, 375, "big"))
for fruit in big_fruit_list:
    all_sprites_list.add(fruit)

gate = Wall((255, 255, 255), 90, 5, 160, 165)
gate_list = pygame.sprite.Group()
all_sprites_list.add(gate)
gate_list.add(gate)
def handleKeyPress(player):
    pygame.event.pump()
    keys = pygame.key.get_pressed()

    if (keys[pygame.K_d]):
        player.moveLeft()
        return
    elif (keys[pygame.K_a]):
        player.moveRight()
        return
    elif (keys[pygame.K_w]):
        player.moveUp()
        return
    elif(keys[pygame.K_s]):
        player.moveDown()
        return
    else:
        player.standStill()
        return
        
pygame.mixer.Sound.play(pacman_intro)
pygame.mixer.music.stop()

starting = 0
while 1:

    for event in pygame.event.get():
        if event.type == pygame.QUIT: sys.exit()

    handleKeyPress(player)
    playerCollideWithFruit(fruit_list, player)
    playerCollideWithBigFruit(big_fruit_list, player)
    for ghost in ghost_list:
        ghost.randomizeMovement()

    updateScreen(player)

    if starting == 0:
        time.sleep(4)
        starting = 1